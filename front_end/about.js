class AboutSection extends HTMLElement {

  static get observedAttributes() {
    return ['name', 'title', 'address', 'email', 'phone'];
  }

  constructor() {
    super();

    // Create a shadow root
    const shadowRoot = this.attachShadow({mode: 'open'});

    // Apply external sytle to the shadow DOM
    const linkElem = document.createElement('link');
    linkElem.setAttribute('rel', 'stylesheet');
//    linkElem.setAttribute('href', 'https://james.christhall.us/resume/about.css');
    linkElem.setAttribute('href', './about.css');
    shadowRoot.appendChild(linkElem);

    // Create the outter shell
    const wrapper = document.createElement('div');
    wrapper.setAttribute('class', 'about-section');

    // TODO: Add Callback for Attrubute Changes

    // Create Title Div
    const title_div = document.createElement('div');
    title_div.setAttribute('id', 'title-div');

    // Append Name
    const name = document.createElement('h1');
    name.setAttribute('id', 'name');
    if (this.hasAttribute('name')) {
      name.innerHTML = this.getAttribute('name');
    }
    title_div.appendChild(name);

    // Append Title
    const title = document.createElement('h5');
    title.setAttribute('id', 'title');
    // Set the text
    if (this.hasAttribute('title')) {
      title.innerHTML = this.getAttribute('title');
    }
    title_div.appendChild(title);

    // Append the Title Div
    wrapper.appendChild(title_div);

    // Create Contact Div
    const contact_div = document.createElement('div');
    contact_div.setAttribute('id', 'contact-div');

    const br = "<br>";
    const contact = document.createElement('p');
    contact.setAttribute('id', 'contact');
    let innerHtml;

    // Append Address
    if (this.hasAttribute('address')) {
      innerHtml = this.getAttribute('address');
      innerHtml += br;
    }

    // Append Phone
    if (this.hasAttribute('phone')) {
      innerHtml += this.getAttribute('phone');
      innerHtml += br;
    }

    // Append Email
    if (this.hasAttribute('email')) {
      innerHtml += this.getAttribute('email');
      innerHtml += br;
    }

    // Fill in the contact section
    contact.innerHTML = innerHtml;

    contact_div.appendChild(contact);

    // Append the Contact Div
    wrapper.appendChild(contact_div);

    // Append the outter shell to the shadow root
    this.shadowRoot.appendChild(wrapper);
  }

  attributeChangedCallback(name, oldVal, newVal) {
    console.log ("Updating " + name);

    const br = "<br>";
    let text;

    switch (name)
    {
      case "name" :
        text = this.shadowRoot.getElementById("name");
        text.innerHTML = newVal;
        break;
      case "title":
        text = this.shadowRoot.getElementById("title");
        text.innerHTML = newVal;
        break;
      case "address":
        text = this.shadowRoot.getElementById("contact");
        text.innerHTML = newVal;
        text.innerHTML += br;
        break;
      case "phone":
        text = this.shadowRoot.getElementById("contact");
        text.innerHTML += newVal;
        text.innerHTML += br;
        break;
      case "email":
        text = this.shadowRoot.getElementById("contact");
        text.innerHTML += newVal;
        text.innerHTML += br;
        break;
    }
//    console.log (this.shadowRoot);
//    this.shadowRoot.querySelector('h1').innerHTML = newVal;
  }


}

customElements.define("about-section", AboutSection);
