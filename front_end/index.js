let baseAddr="https://james.christhall.us/resume/api/"

function setAboutSection() {
  let addr = baseAddr + "get_about";
  let data = fetch(addr, {
    method: 'POST',
    body: JSON.stringify({
      'id':'1'
    })
  }).then(response => {
    return response.json();
  }).then(data => {
    let aboutSection = document.getElementById('about');
    aboutSection.setAttribute('name',    data.name);
    aboutSection.setAttribute('title',   data.title);
    aboutSection.setAttribute('address', data.address);
    aboutSection.setAttribute('phone',   data.phone);
    aboutSection.setAttribute('email',   data.email);
  });
}

function setExperienceList() {
  let addr = baseAddr + "get_experience_list";
  let data = fetch(addr, {
    method: 'POST',
    body: JSON.stringify({
      'id':'1'
    })
  }).then(response => {
    console.log (response);
    return response.json();
  }).then(data => {
    console.log (data);
    let experienceDiv = document.getElementById('experience_list');
    for (let i = 0; i < data.length; i++) {
      // Append experienceList child to experienceDiv
      const experienceSection = document.createElement ('experience_section');
      experienceSection.setAttribute ('id', data[i].experience_id);
      experienceDiv.appendChild (experienceSection);
    }
  });
}

setAboutSection();
setExperienceList();