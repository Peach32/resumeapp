class ExperienceSection extends HTMLElement {

  static get observedAttributes() {
    return ['name', 'title', 'address', 'email', 'phone'];
  }

  constructor() {
    super();

    // Create a shadow root
    const shadowRoot = this.attachShadow({mode: 'open'});

    // Apply external sytle to the shadow DOM
    const linkElem = document.createElement('link');
    linkElem.setAttribute('rel', 'stylesheet');
    linkElem.setAttribute('href', './experience.css');
    shadowRoot.appendChild(linkElem);

}

customElements.define("experience-section", ExperienceSection);
