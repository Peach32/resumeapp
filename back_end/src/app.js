// Express App
var express = require ('express');
var app     = express ();
var cors    = require ('cors');

// Require Routes
var getAbout          = require ('./get_about.js');
var getExperienceList = require ('./get_experience_list');
const port = process.env.PORT;

app.use (express.json({type:['application/json', 'text/plain']}));
app.use (express.urlencoded ({ extended: true }));

// Use Routes for pages
//app.use ('/', api);
app.use ('/get_about', getAbout);
app.use ('/get_experience_list', getExperienceList);

// Start Express App
app.listen (port, localhost => {
  console.log ('Success');
});
