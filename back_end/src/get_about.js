var express = require ('express');
var router  = express.Router ();
const { Client } = require ('pg');

// Supporting Scripts

router.post ('/', function (req, res) {
  // Configure CORS
  res.set('Access-Control-Allow-Origin', '*');

  // Connect to SQLite DB
  const client = new Client({
    host: process.env.PG_HOST,
    port: process.env.PG_PORT,
    user: process.env.PG_USER,
    password: process.env.PG_PASS,
    ssl: {
      rejectUnauthorized: false,
      ca: process.env.PG_CA_CERT
    },
    database: process.env.PG_DATABASE
  });

  client.connect()
    .then (() => {
      console.log ("Connected");
    }).catch (err => {
      console.log ("Error: ");
      console.log (err.stack);
    });

  // TODO: Query User_Id from URL
  // Query ABOUT table for USER_ID
  const aboutQuery = `SELECT * FROM about WHERE id = 1;`;

  let aboutInfo;
  client.query(aboutQuery)
    .then (ans => {
      // Close the database connection
      client.end()

      // Create JSON object with ABOUT_INFO
      aboutInfo = ans.rows[0];

      // Return the JSON object
      res.write(JSON.stringify(aboutInfo));
      res.end();
    }).catch (err => {
      console.log ("ERRORED: ");
      console.log (err.message);
    });
});

module.exports = router;
