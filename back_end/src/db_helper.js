const { Client } = require ('pg');
var helper = {};

getClient = {
  client: new Client({
    host: process.env.PG_HOST,
    port: process.env.PG_PORT,
    user: process.env.PG_USER,
    password: process.env.PG_PASS,
    ssl: {
      rejectUnauthorized: false,
      ca: process.env.PG_CA_CERT
    },
    database: process.env.PG_DATABASE
  }),
  connect: function() {
    client.connect()
      .then(() => {
        console.log ("Connected");
        return client;
      })
      .catch(err => {
        console.log (err.message);
      });
  }
};

module.exports = getClient;