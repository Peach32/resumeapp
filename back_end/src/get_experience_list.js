var express = require ('express');
var router  = express.Router ();
const { Client } = require ('pg');

// Supporting Scripts
//const Board = require('./board.js');
//const getClient = require('./db_helper.js');

router.post ('/', function (req, res) {
  // Configure CORS
  res.set('Access-Control-Allow-Origin', '*');

  console.log (req.body);

  // Connect to the Database
  const client = new Client({
    host: process.env.PG_HOST,
    port: process.env.PG_PORT,
    user: process.env.PG_USER,
    password: process.env.PG_PASS,
    ssl: {
      rejectUnauthorized: false,
      ca: process.env.PG_CA_CERT
    },
    database: process.env.PG_DATABASE
  });

  client.connect()
    .then (() => {
      console.log ("Connected");
    }).catch (err => {
      console.log ("Error: ");
      console.log (err.stack);
    });

  // Get the list of Experience for USER_ID
  const aboutQuery = `SELECT * FROM experience_list WHERE user_id = 1;`;

//  let experience_list;
  client.query(aboutQuery)
    .then (ans => {
      console.log ("Query experience_list");
      // Close the database connection
      client.end();

      console.log (ans.rows);
      // Create JSON object with response
      experience_list = ans.rows;
      console.log (experience_list);

      // Return the JSON object
      res.write(JSON.stringify(experience_list));
      res.end();
    }).catch (err => {
      console.log ("Error experience list");
  //    console.log (err.message);
    });
});

module.exports = router;
